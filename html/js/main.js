/** -----------------------------------------------------

    Start with a namespace utility (via JavaScript Patterns).

    ----------------------------------------------------- */
var MC = MC || {};
MC.namespace = function(nsStr) {
  var parts = nsStr.split('.'),
      parent = MC, i;
  if (parts[0] === "MC") {
    parts = parts.slice(1);
  }
  for (i = 0; i < parts.length; i++) {
    if (typeof parent[parts[i]] === "undefined") {
      parent[parts[i]] = {};
    }
    parent = parent[parts[i]];
  }
  return parent;
};


// Number clamping utility method
(function() {
  Math.clamp = function(val, min, max){
    return Math.min( Math.max(min,val), max);
  };
}());

(function(app, global) {

  // Simple quadratic ease in+out and ease in
  app.namespace('easeInOut');
  app.easeInOut = function(p) {
    var underHalf = (p < 0.5),
        r = underHalf ? p * 2 : (1 - p) * 2;
    r *= r * r;
    return underHalf ? r / 2 : 1 - (r / 2);
  };
  app.namespace('easeIn');
  app.easeIn = function(p) {
    var r = p * 2;
    r *= r * r;
    return r / 2;
  };

  /** -----------------------------------------------------

      Bristle

      options:

        - x:         Integer (required)
        - y:         Integer (required)
        - width:     Integer (required)
        - direction: Integer (required) -1 and 1 are valid values

      ----------------------------------------------------- */
  app.namespace('Bristle');
  app.Bristle = (function() {

    var MIN_DIST = 20,
        MAX_DIST = 85,

        Bristle;

    Bristle = function(options) {

      var dist    = MIN_DIST + Math.floor(Math.random() * (MAX_DIST - MIN_DIST)),
          width   = options.width,
          x       = options.x,
          startY  = options.y,
          destY   = options.y + dist * options.direction,
          lastY   = options.y,
          currY   = options.y,
          opacity = 1;

      this.update = function(perc) {
        var travelPerc = app.easeInOut(perc),
            endY       = startY + (travelPerc * (destY - startY)),
            height     = endY - lastY,
            mark       = {
              x: x,
              y: lastY,
              width: width,
              height: height,
              opacity: app.easeIn(1 - perc)
            };

        if(options.direction === -1) {
          mark.height *= -1;
          mark.y = endY;
        }

        lastY = endY;

        return mark;
      };

    };

    Bristle.prototype = {
      constructor: app.Bristle
    };

    return Bristle;

  }());

  /** -----------------------------------------------------

      Brushstroke - a grouping of Bristles

      options:

        - canvasW: Integer (required)
        - canvasH: Integer (required)
        - color:   Object {r, g, b} (required), each component is 0 - 255
        - x:       Integer (optional)
        - y:       Integer (optional)
    
      ----------------------------------------------------- */
  app.namespace('Brushstroke');
  app.Brushstroke = (function() {

    var MIN_DUR      = 300,
        MAX_DUR      = 1500,
        MIN_WIDTH    = 10,
        MAX_WIDTH    = 40,
        MIN_BRISTLES = 3,
        MAX_BRISTLES = 8,
        strokeId     = 0,
        
        Brushstroke;

    Brushstroke = function(options) {

      var Bristle   = app.namespace('Bristle'),
          id        = ++strokeId,
          isDone    = false,
          direction = Math.random() < 0.5 ? -1 : 1,
          x         = (typeof options['x'] === 'undefined') ? Math.floor(Math.random() * options.canvasW) : options.x,
          y         = (typeof options['y'] === 'undefined') ? Math.floor(Math.random() * options.canvasH) : options.y,
          width     = MIN_WIDTH + (Math.floor(Math.random() * (MAX_WIDTH - MIN_WIDTH))),
          dur       = MIN_DUR + (Math.floor(Math.random() * (MAX_DUR - MIN_DUR))),
          startTime = Date.now(),
          endTime   = startTime + dur,
          color     = {
            r: Math.clamp(options.color.r, 0, 255),
            g: Math.clamp(options.color.g, 0, 255),
            b: Math.clamp(options.color.b, 0, 255)
          },
          bristles  = [],
          numBristles = MIN_BRISTLES + Math.floor(Math.random() * (MAX_BRISTLES - MIN_BRISTLES)),
          xStart    = x - (width / 2),
          bristleW  = width / numBristles,
          bristleX,
          bristle,
          i;

      // Generate the bristles that make up the brushstroke.
      bristleX = xStart;
      for(i = 0; i < numBristles; i++) {
        bristle = new Bristle({
          width:     bristleW,
          x:         bristleX,
          y:         y,
          direction: direction,
          color: {
            r: color.r,
            g: color.g,
            b: color.b
          }
        });

        bristles.push(bristle);

        bristleX += bristleW;
      } // end bristle generation

      // --------------
      // PUBLIC METHODS
      this.isDone = function() {
        return isDone;
      };

      this.getColor = function() {
        return {
          r: color.r,
          g: color.g,
          b: color.b
        };
      };

      this.getPercent = function() {
        var now  = Date.now();
        var perc = (now - startTime) / dur;
        return Math.clamp(perc, 0, 1);
      };

      this.update = function() {
        var perc = this.getPercent(),
            bristle,
            i,
            mark,
            marks = [];

        if(!isDone) {
          for(i = 0; i < numBristles; i++) {
            bristle = bristles[i];
            marks.push(bristle.update(perc));
          }

          if(perc >= 1) {
            isDone = true;
          }
        }

        return marks;
      };

      this.getId = function() {
        return id;
      };

    };

    Brushstroke.prototype = {
      constructor: app.Brushstroke
    };

    return Brushstroke;

  }());


  /** -----------------------------------------------------

      KineticPainting (Main application)
    
      ----------------------------------------------------- */
  app.kineticPainting = (function() {

    var SRC_W        = 200,
        SRC_H        = 200,
        NUM_COLORS   = 100,
        CANVAS_COLOR = '#3e4b54',
        CANVAS_W,
        CANVAS_H,
        Brushstroke  = app.namespace('Brushstroke'),
        canvas,
        ctx,
        SRC_IMG_PATH = 'img/source.jpg',
        srcImg,
        srcImgData,
        animFrameId  = -1,
        autoBrushstrokeId = -1,
        brushstrokes,
        self,
        colorLookup,
        resetButton,
        pauseButton,
        MIN_TIME_BETWEEN_MANUAL_MARKS = 200,
        lastManualMarkTime = -1,
        isMouseDown = false,
        isPaused = false,
        sampleScaleX, sampleScaleY;

    // ---------------
    // Private methods
    var clearCanvas = function() {
      if(ctx) {
        ctx.fillStyle = CANVAS_COLOR;
        ctx.fillRect(0, 0, CANVAS_W, CANVAS_H);
      }
    };

    var clearRenderId = function() {
      if(animFrameId !== -1) {
        cancelAnimationFrame(animFrameId);
        animFrameId = -1;
      }
    };

    var render = function() {
      var i, j,
          doneIndex,
          brushstroke,
          marks,
          mark,
          numMarks,
          color,
          doneStrokeIndices = [],
          numDoneStrokes = 0,
          removedStrokes,
          numBrushstrokes = brushstrokes.length;

      clearRenderId();

      for(i = 0; i < numBrushstrokes; i++) {
        brushstroke = brushstrokes[i];
        color = brushstroke.getColor();
        marks = brushstroke.update();

        numMarks = marks.length;
        for(j = 0; j < numMarks; j++) {
          mark = marks[j];
          ctx.fillStyle = 'rgb(' + color.r + ',' + color.g + ',' + color.b + ',' + mark.opacity + ')';
          ctx.fillRect(mark.x, mark.y, mark.width, mark.height);
        }

        if(brushstroke.isDone()) {
          doneStrokeIndices.push(i);
        }
      }

      // Loop through and remove all done strokes
      numDoneStrokes = doneStrokeIndices.length;
      for(i = numDoneStrokes - 1; i >= 0; i--) {
        doneIndex = doneStrokeIndices[i];
        removedStrokes = brushstrokes.splice(doneIndex, 1);
      }

      animFrameId = requestAnimationFrame(render);
    };

    var onAutoBrushstrokeRequest = function() {
      var numBrushstrokes = 3 + Math.floor((Math.random() * 5));
      while(numBrushstrokes) {
        self.generateBrushstroke();
        numBrushstrokes--;
      }
    };

    // --------------
    // Public methods
    self = {

      init: function() {
        var r, g, b, i,
            srcMap,
            grad,
            completeInitialization;

        canvas = document.querySelector('#canvas');
        resetButton = document.querySelector('#reset');
        pauseButton = document.querySelector('#pause');
        ctx = canvas.getContext('2d');
        CANVAS_W = ctx.canvas.width;
        CANVAS_H = ctx.canvas.height;
        sampleScaleX = SRC_W / CANVAS_W;
        sampleScaleY = SRC_H / CANVAS_H;
        brushstrokes = [];

        completeInitialization = function() {
          srcImg.removeEventListener('load', completeInitialization);

          // Render our source image and create a scaled down version for sampling
          ctx.drawImage(srcImg, 0, 0, SRC_W, SRC_H);
          srcImgData = ctx.getImageData(0, 0, SRC_W, SRC_H);

          clearCanvas();

          // Create a gradient that we'll use as our lookup table for color mapping
          colorLookup = [];
          grad = ctx.createLinearGradient(0, 0, NUM_COLORS, 1);
          grad.addColorStop(0,    '#030d28');
          grad.addColorStop(0.33, '#394f5b');
          grad.addColorStop(0.67, '#7fb5a8');
          grad.addColorStop(1,    '#e6f6fa');
          ctx.fillStyle = grad;
          ctx.fillRect(0, 0, NUM_COLORS, 1);
          srcMap = ctx.getImageData(0, 0, NUM_COLORS, 1);
          for(i = 0; i < NUM_COLORS; i++) {
            r = srcMap.data[4 * i];
            g = srcMap.data[4 * i + 1];
            b = srcMap.data[4 * i + 2];
            colorLookup.push({
              r: r,
              g: g,
              b: b
            });
          }
          
          clearCanvas();

          // Add interaction to the page
          resetButton.addEventListener('click', function(e) {
            self.reset();
          });

          pauseButton.addEventListener('click', function(e) {
            if(isPaused) {
              pauseButton.textContent = 'Pause';
              self.startAutoBrushstrokeGenerator();
            } else {
              pauseButton.textContent = 'Resume';
              self.stopAutoBrushstrokeGenerator();
            }
            isPaused = !isPaused;
          });

          canvas.addEventListener('mousedown', function(e) {
            isMouseDown = true;
            self.stopAutoBrushstrokeGenerator();
          });

          window.addEventListener('mouseup', function(e) {
            isMouseDown = false;
            if(!isPaused) {
              self.startAutoBrushstrokeGenerator();
            }
          });

          canvas.addEventListener('mousemove', function(e) {
            if(isMouseDown) {
              if(e.offsetX >= 0 && e.offsetX <= CANVAS_W && e.offsetY >= 0 && e.offsetY <= CANVAS_H) {
                if(Date.now() - lastManualMarkTime > MIN_TIME_BETWEEN_MANUAL_MARKS) {
                  self.generateBrushstroke(e.offsetX, e.offsetY);
                }
              }
            }
          });

          canvas.addEventListener('click', function(e) {
            self.generateBrushstroke(e.offsetX, e.offsetY);
          });

          // Start the render callback and kick off the painting
          render();
          self.startAutoBrushstrokeGenerator();
        };

        // Load our source image.
        srcImg = new Image();
        srcImg.addEventListener('load', completeInitialization);
        srcImg.addEventListener('error', function() {
          console.error("Unable to load image: " + SRC_IMG_PATH);
        });
        srcImg.src = SRC_IMG_PATH;

        // Override the init declaration to prevent multiple init() calls.
        this.init = function() {
          console.log("Already initialized.");
        };
      },

      startAutoBrushstrokeGenerator: function() {
        this.stopAutoBrushstrokeGenerator();
        autoBrushstrokeId = setInterval(onAutoBrushstrokeRequest, 75);
      },

      stopAutoBrushstrokeGenerator: function() {
        if(autoBrushstrokeId !== -1) {
          clearInterval(autoBrushstrokeId);
          autoBrushstrokeId = -1;
        }
      },

      generateBrushstroke: function(x, y) {
        var sampleX, sampleY,
            srcIndex,
            pxR, pxG, pxB,
            lum,
            col,
            brushStroke;

        if(typeof x === 'undefined') {
          x = Math.floor(Math.random() * CANVAS_W);
        }
        if(typeof y === 'undefined') {
          y = Math.floor(Math.random() * CANVAS_H);
        }
        x = Math.clamp(x, 0, CANVAS_W);
        y = Math.clamp(y, 0, CANVAS_H);

        // Sample the color + luminance from our src image.
        sampleX = Math.floor(sampleScaleX * x);
        sampleY = Math.floor(sampleScaleY * y);
        srcIndex = sampleY * (SRC_W * 4) + sampleX * 4;
        pxR = srcImgData.data[srcIndex];
        pxG = srcImgData.data[srcIndex + 1];
        pxB = srcImgData.data[srcIndex + 2];
        lum = ((pxR / 255) * 0.2126) + ((pxG / 255) * 0.7152) + ((pxB / 255) * 0.0722); // Luminance calculation
        
        // Based on the luminance, we'll pick from our colors lookup
        lum = Math.floor(lum * (NUM_COLORS - 1));
        col = colorLookup[lum];

        brushstroke = new Brushstroke({
          canvasW: CANVAS_W,
          canvasH: CANVAS_H,
          color:   {
            r: col.r,
            g: col.g,
            b: col.b
          },
          x: x,
          y: y
        });

        brushstrokes.push(brushstroke);
      },

      reset: function() {
        clearRenderId();
        brushstrokes = [];
        clearCanvas();
        render();
      }

    };

    return self;

  }());

}(MC, this));

// Let's go!
MC.kineticPainting.init();
Kinetic Painting
=======================================================================

Project Brief
-------------

Take a photo of an object in your environment, and recreate that object using only HTML/JS/CSS. Don't spend more than a day on the project.


Project Concept
-------------

Taking inspiration from the mark-making and brushwork of some recent paintings of mine, I thought it might be interesting to create a little program that would use a reference photo (i.e. the photo of an object) and attempt to re-create it by employing a visually similar mark-making technique.

Rather than simply rendering a final image all in one pass (and probably locking up the user's web browser for multiple seconds), I thought it would be much more interesting to watch the image build over time. So brushstrokes were animated, and there is no definitive "end" to the generated image. Even after full coverage with "paint", the image remains visually active and alive.

Also, I realize the skull might seem a little morbid, but it's just a plastic replica that I use for study and guidance in portraiture.


How To Run
-------------

Because the app makes use of the HTML5 Canvas API's ability to read pixel data from an image that's been drawn to the canvas, it must be run from a local or remote web server. It will not run from the local filesystem.

For your convenience, I have hosted an instance of the submitted project here: [http://mikecreighton.com/kineticpainting](http://mikecreighton.com/kineticpainting)

Let it run for about a minute or so, and the photographed object will eventually emerge.


What It Can Do
-------------

**RESET Button**
  - Clicking will wipe the canvas clean

**PAUSE / RESUME Button**
  - Clicking will toggle the automated brushstrokes

**Click + Drag on Canvas**
  - Allows user to take over the mark-making and spawn new brushstrokes while the mouse is held down. Any automated brushstrokes will pause while the user is interacting with the surface.


Future Thoughts & Considerations
-------------

  - Make it possible for user to use their own photo via file input
  
  - Appropriately handle source images of varying sizes
  
  - Create UI widget for creating own color palette gradient map
  
  - Add a couple more types of Brushstrokes to create a bit more visual variety and help add points of interest
  
  - Performance optimizations (specifically possibly replacing arrays of Brushstrokes / Bristles with hashmaps in order to replace for loops with for..in loops)
  
  - Add more sophisticated user-directed brushstrokes (and possibly ability to pick different brush types, use specific colors from the gradient color map)
  
  - Add ability to save image to disk
  
  - Increase Canvas resolution for print-ready file exporting
  
  - Re-create this as a more fully-functional app using Electron
  
  - Possibly tap into PointerEvents to make use of Wacom pen pressure sensitivity and tilt values

